# Computer setup

This repository represents the current state of your work machine. Keep it
updated for easy migrations.

## A word on version managers

Generally, managing versions **should** be handled by `asdf`. However, multiple
known languages use their own managers and do not easily allow for building
things with only one version manager. Those are:

| Language | Version manager | Source                         |
| -------- | --------------- | ------                         |
| Rust     | rustup          | https://www.rustup.rs/         |
| Ruby     | rvm             | https://rvm.io/                |
