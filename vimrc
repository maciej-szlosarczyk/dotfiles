set nocompatible
filetype off
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
set rtp+=~/.dotfiles/vim/bundle/Vundle.vim

cd ~
set encoding=utf-8

" Vundle
call vundle#begin('~/.dotfiles/vim/bundle')
Plugin 'gmarik/Vundle.vim' " Vundle
" Themes
Plugin 'cohlin/vim-colorschemes' " Colorschemes
Plugin 'chriskempson/base16-vim'
Plugin 'endel/vim-github-colorscheme'
" Tools
Plugin 'scrooloose/nerdtree' " Nerdtree
Plugin 'bling/vim-airline' " Airline
Plugin 'vim-airline/vim-airline-themes' " Airline themes
Plugin 'vim-scripts/TaskList.vim' " Tasklist
Plugin 'vim-scripts/minibufexpl.vim' " MiniBufferExplorer
" Linters and snippet generators
Plugin 'vim-scripts/snipMate' " Snipmate
Plugin 'scrooloose/syntastic' " Syntastic
Plugin 'aklt/vim-line_length' " Line_length
" Markup langauge support
Plugin 'davidbeckingsale/writegood.vim' " Write-good
Plugin 'goldfeld/criticmarkup-vim' " CriticMarkup
Plugin 'godlygeek/tabular' " Dependency for markdown
Plugin 'plasticboy/vim-markdown' " Markdown
Plugin 'othree/xml.vim' " XML
Plugin 'vim-scripts/vim-vagrant' " Vagrant (ruby)
" Programming languages
Plugin 'moll/vim-node' " NodeJS
Plugin 'tpope/vim-rails' " Vim.rails
Plugin 'vim-ruby/vim-ruby' "Vim-ruby
Plugin 'elixir-lang/vim-elixir' "Elixir
Plugin 'slashmili/alchemist.vim' "More Elixir



call vundle#end()

" NERDTree
autocmd StdinReadPre * let s:std_in=1
let NERDTreeShowBookmarks=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
nmap <silent> <C-D> :NERDTreeToggle<CR>
set autochdir
let NERDTreeChDirMode=2

" Swap files
set swapfile
set dir=~/vimfiles/tmp

" No annoying backup files
set nobackup
set nowritebackup

" Autocompletion
filetype plugin indent on
set omnifunc=syntaxcomplete#Complete

" Line length
let g:LineLength_LineLength = 200

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

" Eyecandy
let base16colorspace=256
set guifont=Monaco:h11
colorscheme base16-ashes
let g:airline_theme='darcula'
let g:enable_bold_font = 1
set number
