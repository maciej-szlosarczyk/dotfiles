# Make sure you use 256 colors
export TERM=xterm-256color

# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="ys"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git brew rvm mix zsh-iterm-plugin)

# User configuration

# export PATH="/usr/bin:/bin:/usr/sbin:/sbin:$PATH"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# User settings #
#################

export CLICOLOR=1;

# Use completions from brew #
#############################
fpath=(/usr/local/share/zsh-completions $fpath)

autoload -U compinit && compinit
zmodload -i zsh/complist
compinit
zstyle ':completion:*' list-colors 'exfxcxdxbxegedabagacad'

setopt autocd

# Command history #
setopt incappendhistory # Add history
setopt histignoredups # Do not add multiple usage of the same command
setopt extendedhistory # Add time
export HISTSIZE=1000 # Save a 1000
export SAVEHIST=1000 # =//=
export HISTFILE=~/.zsh_history # Where to save it.

# Detect if Mac or Linux
platform='unknown'
unamestr=`uname`

if [[ $unamestr == 'Linux' ]]; then
   platform='linux'
elif [[ $unamestr == 'Darwin' ]]; then
   platform='mac'
fi

# Aliases for linux (including vagrant boxes)
if [[ $platform == 'linux' ]]; then
   alias ls='ls --color=auto -l'
   # Alias for RubyMine remote debugger
   alias remote_debug='rdebug-ide --host 0.0.0.0 --port 1234 --dispatcher-port 61234 -- bin/rails s -b 0.0.0.0'
elif [[ $platform == 'mac' ]]; then
   alias ls='ls -l -h'
   alias e='/Applications/Emacs.app/Contents/MacOS/Emacs -nw'
   alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs'
fi

# Useful functions
[[ -s "$HOME/.dotfiles/functionsrc" ]] && source "$HOME/.dotfiles/functionsrc"

# Aliases are kept separately
[[ -s ~/.dotfiles/aliasrc ]] && source ~/.dotfiles/aliasrc

# Specific config for Inbank
[[ -s ~/.dotfiles/inbankrc ]] && source ~/.dotfiles/inbankrc

# Base16 colors
# BASE16_SHELL="$HOME/.config/base16-shell/scripts/base16-ashes.sh"
# [[ -s $BASE16_SHELL ]] && source $BASE16_SHELL

# Rust
source $HOME/.cargo/env

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Asdf
. $HOME/.asdf/asdf.sh
. $HOME/.asdf/completions/asdf.bash

# GO
export GOPATH=$HOME/Development/go
export PATH=$PATH:$GOPATH/bin
export GOROOT=$HOME/.asdf/installs/golang/1.9.2/go
export PATH="/usr/local/opt/qt@5.5/bin:$PATH"
