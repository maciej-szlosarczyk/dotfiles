#! /usr/bin/env
# Run this script to set up a new computer

echo "###############################"
echo "# Setup emacs"
echo '###############################'
git clone git@github.com:maciej-szlosarczyk/emacs.git ~/.emacs.d/
ln -s ~/.emacs.d/emacs ~/.emacs

echo '###############################'
echo '# Setup git'
echo '###############################'
rm ~/.gitconfig
rm ~/.gitignore_global
ln -s gitconfig ~/.gitconfig
ln -s gitignore_global ~/.gitignore_global

echo '###############################'
echo '# All set, thanks!'
echo '###############################'
