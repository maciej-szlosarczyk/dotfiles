# !/usr/env/bin/bash
echo "####################################"
echo "# Setting your Mac"
echo "####################################"

echo "####################################"
echo "# Increase keyboard response speed"
echo "####################################"
defaults write -g KeyRepeat -int 1
defaults write -g InitialKeyRepeat -int 10

echo "####################################"
echo "# Installing homebrew and cask"
echo "####################################"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew tap caskroom/cask

echo "####################################"
echo "# Installing zsh and other utils"
echo "####################################"
for i in $(cat brews.txt);
do
    brew install "$i";
done

for i in $(cat casks.txt);
do
    brew cask install "$i";
done

echo "##################################"
echo "# Install nginx"
echo "##################################"
brew uninstall nginx
brew install nginx --with-passenger

echo "###############################"
echo "# Setup zsh"
echo '###############################'
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
rm ~/.zshrc
chsh -s $(which zsh)
git clone https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell

echo "##################################"
echo "# Setting up emacs"
echo "##################################"
git clone git@github.com:maciej_szlosarczyk@emacs.git ~/.emacs.d/

echo "##################################"
echo "# Install exuberant-ctags"
echo "##################################"
brew uninstall ctags
brew install ctags-exuberant
brew link --overwrite ctags

echo "##################################"
echo "# Create folder structure"
echo "##################################"
mkdir -p ~/Development/{haskell,ruby,elixir,java,python,rust}

echo "##################################"
echo "# Install Ruby"
echo "##################################"
curl -sSL https://get.rvm.io | bash
source ~/.rvm/scripts/rvm

echo "##################################"
echo "# Install Elixir"
echo "##################################"
curl -sSL https://raw.githubusercontent.com/taylor/kiex/master/install | bash -s
[[ -s "$HOME/.kiex/scripts/kiex" ]] && source "$HOME/.kiex/scripts/kiex"

echo "##################################"
echo "# Install other languages"
echo "##################################"
curl https://sh.rustup.rs -sSf | sh
source ~/.cargo/env

echo "##################################"
echo "# Add iterm configuration file"
echo "##################################"
defaults write com.googlecode.iterm2.plist PrefsCustomFolder -string "~/.dotfiles/iterm2"
defaults write com.googlecode.iterm2.plist LoadPrefsFromCustomFolder -bool true

echo "##################################"
echo "# Configure asdf"
echo "# and install latest versions"
echo "##################################"
langs=('erlang' 'elixir' 'nodejs')
for i in "${langs[@]}";
do
    asdf plugin-add "$i";
    if [[ "$i" == "nodejs" ]]
    then
        /usr/local/opt/asdf/plugins/nodejs/bin/import-release-team-keyring
    fi;

    latest=$(asdf list-all "$i" | tail -1)
    asdf install "$i" "$latest"
    asdf global "$i" "$latest"
done

asdf plugin-add python

echo "##################################"
echo "# Create symlinks"
echo "##################################"
ln -s ~/.dotfiles/zshrc ~/.zshrc
ln -s ~/.dotfiles/gitconfig ~/.gitconfig
ln -s ~/.dotfiles/gitignore_global ~/.gitignore_global
ln -s ~/.dotfiles/guard.rb ~/.guard.rb
ln -s ~/.dotfiles/ctags ~/.ctags
ln -s ~/.dotfiles/tool-version ~/.tool-versions
# TODO: Write a script that automates installation of pgcli

echo "##################################"
echo "# All set and done. Be happy!"
echo "##################################"
